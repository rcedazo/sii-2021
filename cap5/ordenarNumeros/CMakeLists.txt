# Indicamos el nombre que queremos que tenga nuestro proyecto
PROJECT(OrdenarNumero)

# Indicamos que el proyecto va a crear una librer�a
# e indicamos los archivos que van a dar lugar a la libreria
# Deben estar creados antes de ejecutar CMake
ADD_EXECUTABLE(OrdenarNumero
	fuentes/OrdenarNumeros/OrdenarNumeros.cpp
	fuentes/Visualizador/GUIOrdenarNumeros.cpp
)


TARGET_LINK_LIBRARIES(OrdenarNumero
)

CMAKE_MINIMUM_REQUIRED(VERSION 2.6)