// De: "Apuntes de Sistemas Informáticos Industriales" Carlos Platero.
// (R) 2013 Con licencia GPL.
// Ver permisos en licencia de GPL


#ifndef _INC_CNOMBRE_
#define _INC_CNOMBRE_

#include <string.h>
#include "INombre.h"

class CNombre : public INombre 
{
public:
	friend class INombre;
 
	 virtual void setNombre(const char *cadena)
	 { strcpy (elNombre, cadena); }

	 virtual const char * getNombre (void)
	 { return (elNombre);}

private:
	char elNombre[80];
	CNombre() {}
};

#endif

