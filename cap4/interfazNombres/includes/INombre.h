// De: "Apuntes de Sistemas Informáticos Industriales" Carlos Platero.
// (R) 2013 Con licencia GPL.
// Ver permisos en licencia de GPL

#ifndef _INOMBRE_INC_
#define _INOMBRE_INC_

typedef enum {ESTANDAR_STL, ESTILO_C, CADENA_MFC} tipoTiraCaracteres;

class INombre {
public:
	virtual void setNombre (const char *) = 0;
	virtual const char * getNombre () = 0;

	//Factoria de objetos
	static INombre *factoriaObjetos(tipoTiraCaracteres);

};

#endif
