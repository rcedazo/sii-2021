// Bloque.cpp: implementation of the Bloque class.
//
//////////////////////////////////////////////////////////////////////

#include "..\..\INCLUDES\DOMINIO\Bloque.h"
#include "..\..\INCLUDES\comunes\glut.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Bloque::Bloque()
{
}

Bloque::~Bloque()
{


}

void Bloque::dibuja()
{
	glColor3ub(255,255,0);
	glTranslatef(x,y,z);
	glutSolidCube(base);
	glTranslatef(-x,-y,-z);
}

