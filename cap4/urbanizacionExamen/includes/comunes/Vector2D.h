// Vector2D.h: interface for the Vector2D class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VECTOR2D_H__86ED85F9_9ED7_4672_9273_C1BB38271909__INCLUDED_)
#define AFX_VECTOR2D_H__86ED85F9_9ED7_4672_9273_C1BB38271909__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class Vector2D  
{
public:
	Vector2D();
	virtual ~Vector2D();
	float x;
	float y;
};

#endif // !defined(AFX_VECTOR2D_H__86ED85F9_9ED7_4672_9273_C1BB38271909__INCLUDED_)
