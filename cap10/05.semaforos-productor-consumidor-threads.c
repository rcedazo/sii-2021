// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <semaphore.h>

#define BUFF_SIZE 10
#define TOTAL_DATOS 25

sem_t n_datos;
sem_t n_huecos;

int buffer[BUFF_SIZE];

void Productor(void) {
	int ip, dato;
	for (ip=0; ip<TOTAL_DATOS; ip++) {
		sleep(5);
		dato = ip;
		printf("Dato producido: %d\n", dato);
		sem_wait(&n_huecos);
		buffer[ip%BUFF_SIZE]=dato;
		sem_post(&n_datos);	
	}
}

void Consumidor(void) {
	int ic, dato;
	for (ic=0; ic<TOTAL_DATOS; ic++) {		
		sem_wait(&n_datos);
		dato = buffer[ic%BUFF_SIZE];
		sem_post(&n_huecos);
		printf("Dato consumido: %d\n", dato);
		//sleep(5);	
	}
}

int main() {
	pthread_t th1, th2;

	// Inicializo los semáforos
	sem_init(&n_datos, 0, 0);
	sem_init(&n_huecos, 0, BUFF_SIZE);

	pthread_create(&th1, NULL, Productor, NULL);
	pthread_create(&th2, NULL, Consumidor, NULL);
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);

	sem_destroy(&n_datos);
	sem_destroy(&n_huecos);

	return 0;
}
