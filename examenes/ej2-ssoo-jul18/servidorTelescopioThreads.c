// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/time.h>   
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>

// Semaforo para acceso a la seccion critica (ra y dec)
sem_t en_exclusiva;

float ra = 0.0, dec = 0.0;

int tuberia;

// Thread que lee los datos del telescopio cada 5 segundos
void Monitor(void) {
	char mensaje[100];
	while(1) {
		// SECCION CRITICA PARA LEER RA Y DEC
		sem_wait(&en_exclusiva);
		printf("Lee los datos del telescopio\n");
		// El monitor devuelve los ultimos valores de RA y DEC (variables globales)
		// No consulta la montura para obtener los valores
		printf("Telescopio en la posicion RA=%.2f, DEC=%.2f\n", ra, dec);
		// FIN DE LA SECCION CRITICA
		sem_post(&en_exclusiva);
		// Se envian las coordenadas al controlador del telescopio 
		// a traves de la tuberia FIFO
		sprintf(mensaje,"%.2f %.2f", ra, dec);
		write(tuberia, mensaje, strlen(mensaje)+1);
		sleep(5);
	}
}

// Thread servidor que atiende peticiones para mover el telescopio
// Comunicacion por sockets
void Servidor(void) {

  struct sockaddr_in server_addr,client_addr;
  int sd, sc, res, size;
  int num[2];

  sd = socket(AF_INET, SOCK_STREAM, 0);
  if (sd < 0) {
    printf("Error en la llamada socket\n");
    return 1;
  }

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(4200);

  int on=1;
  setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));

  if (bind(sd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
    printf("Error en la llamada bind\n");
    return 1;
  }
  listen(sd, 5);
  
  size = sizeof(client_addr);
  
  while (1){
    printf("Servidor del telescopio esperando cliente...\n");
    sc = accept(sd, (struct sockaddr *)&client_addr, &size);

    if (sc < 0){
      printf("Error en accept\n");
      break;
    }

    /* recibe la petición, dos números enteros */
    if (read(sc, (char *) num, 2 *sizeof(int)) < 0){
      printf("Error en read\n");
      pthread_exit(1);
    }
    // INICIO DE LA SECCION CRITICA
    sem_wait(&en_exclusiva);
    /* los datos se transforman del formato de red al del computador */
    ra = ntohl(num[0]);
    dec = ntohl(num[1]);
    printf("Empieza movimiento a RA=%.2f, DEC=%.2f\n", ra, dec);
    sleep(10);	// Se actualizan ra y dec
    printf("Fin del movimiento\n");
    // FIN DE LA SECCION CRITICA
    sem_post(&en_exclusiva);
  }
  close (sd);
  pthread_exit(1);

}

int main() {
	pthread_t th1, th2;

	// Se crea la tuberia para 
	mkfifo("/tmp/Tuberia",0777);
	// Se abre la tuberia 
	tuberia=open("/tmp/Tuberia",O_WRONLY);

	// Inicializo el semáforo
	sem_init(&en_exclusiva, 0, 1);

	pthread_create(&th1, NULL, Servidor, NULL);
	pthread_create(&th2, NULL, Monitor, NULL);
	pthread_join(th1, NULL);
	pthread_join(th2, NULL);

	sem_destroy(&en_exclusiva);

	close(tuberia);
	unlink("/tmp/Tuberia");

	return 0;
}
