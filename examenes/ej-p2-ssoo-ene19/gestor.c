// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/time.h>   
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <pthread.h>
#include <fcntl.h>
#include <semaphore.h>

pthread_mutex_t mutex;

float sensor1, sensor2;
int alarma = 0; // 0 off - 1 on

int tuberia;

struct Tmensaje{
    int id; 	    // id del sensor
    float temp;	    // valor de temperatura
    long timestamp; // hora actual en microsegundos
};

// Recibe de la tuberia la informacion de los sensores
void GestorSensores(void) {
	struct Tmensaje mensaje; 

	// Se crea la tuberia
	mkfifo("/tmp/Tuberia",0777);
	// Se abre la tuberia 
	tuberia=open("/tmp/Tuberia",O_RDONLY);

	while(1) {
		read(tuberia, &mensaje, sizeof(mensaje));
		printf("Sensor recibido: %d - %0.2f grados [%lu]\n", mensaje.id, mensaje.temp, mensaje.timestamp);
		// SECCION CRITICA CON MUTEX		
		pthread_mutex_lock(&mutex);
		if (mensaje.id == 1)
			sensor1 = mensaje.temp;
		else if (mensaje.id == 2)
			sensor2 = mensaje.temp;
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(1);
}

void Alarma() {
   float media, umbral = 5; // 5 grados
   while(1) {
      sleep(10); // cada 10 segundos calcula la media
      // SECCION CRITICA CON MUTEX
      pthread_mutex_lock(&mutex);
      media = (sensor1 + sensor2) / 2;
      printf("Media = %f\n", media);
      if (media <= umbral)
	alarma = 1;
      pthread_mutex_lock(&mutex);
   }
}

int main() {
	pthread_t th1, th2;

        pthread_mutex_init(&mutex, NULL);

	pthread_create(&th1, NULL, GestorSensores, NULL);
	pthread_create(&th2, NULL, Alarma, NULL);

	pthread_join(th1, NULL);
	pthread_join(th2, NULL);

	pthread_mutex_destroy(&mutex);

	close(tuberia);
	unlink("/tmp/Tuberia");

	return 0;
}
